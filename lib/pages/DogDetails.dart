import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/DogBreedHeader.dart';
import 'package:flutter_app/domain/Breed.dart';
import 'package:http/http.dart' as http;

class DogDetails extends StatefulWidget{

  final Breed _breed;
  DogDetails(this._breed);
  @override
  State<StatefulWidget> createState() {
    return _DogDetails(this._breed);
  }
}

class _DogDetails extends State<DogDetails>{
  FadeInImage _image;
  Breed _breed;
  _DogDetails(_breed);

  void getDogImage() async {
    print("breed");
    print(_breed);
    String url = "https://dog.ceo/api/breed/" + _breed.name + "/images/random";

    http.Response response = await http.get(url);
    if (response.statusCode != 200) {
      setState(() {
        _image = FadeInImage(
            placeholder: AssetImage("assets/loadingImage.png"),
            image: AssetImage("assets/errorImage.png")
        );
      });
      return;
    }

    Map<String, dynamic> responseMap = json.decode(response.body);
    if (responseMap["status"] != "success") {
      setState(() {
        _image = FadeInImage(
            placeholder: AssetImage("assets/loadingImage.png"),
            image: AssetImage("assets/errorImage.png")
        );
      });
      return;
    }

    setState(() {
      _image = FadeInImage.assetNetwork(
          placeholder: "assets/loadingImage.png",
          image: responseMap["message"]
      );
    });
  }

  @override
  void initState() {
    super.initState();
    getDogImage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_breed.name),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            DogBreedHeader(breed: _breed),
            _image ?? Image.asset("assets/loadingImage.png")
          ],
        ),
      ),
    );
  }
}